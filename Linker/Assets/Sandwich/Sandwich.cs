﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sandwich : MonoBehaviour
{
    private void Start()
    {
        SimpleIngredient mayo = new SimpleIngredient("мазик", 20, 5, 109);
        SimpleIngredient kepchuk = new SimpleIngredient("кепчук", 20, 22, 76);
        SimpleIngredient bread = new SimpleIngredient("хлеб", 20, 20, 20);
        SimpleIngredient cheese = new SimpleIngredient("сыр", 20, 20, 20);
        SimpleIngredient kolbasa = new SimpleIngredient("колбаса", 20, 20, 20);

        ComplexIngredient ketchinez = new ComplexIngredient("кетчинез",new List<Ingredient> {mayo, kepchuk });
        ComplexIngredient sandwich = new ComplexIngredient("Сэндвич",new List<Ingredient> {bread, ketchinez , cheese, kolbasa, bread });

        Debug.Log("kall :" + sandwich.GetTotalCalories());
        Debug.Log("Weight :" + sandwich.GetTotalWeight());
        Debug.Log("Coast :" + sandwich.GetTotalCost());
    }
}
